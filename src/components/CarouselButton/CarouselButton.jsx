import "./CarouselButton.css"

const CarouselButton = ({onClick, children}) => {
	return <button className="carousel-button" onClick={onClick}>{children}</button>
} 

export default CarouselButton;