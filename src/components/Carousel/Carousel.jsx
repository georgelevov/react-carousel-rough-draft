// Disabled since code is only for demo purposes
/* eslint-disable react/prop-types */

// initialIndex?: number, defaults to 0
// transitionDuration?: number, defaults to 400ms
// autoplay?: boolean, defaults to true
// autoplayInterval?: number, defaults to 3000ms
// infiniteLoop?: boolean, defaults to true
// onPageChange?(index: number): void;

import { useState, useEffect } from "react";
import CarouselItem from "../CarouselItem/CarouselItem";
import CarouselButton from "../CarouselButton/CarouselButton";
import "./Carousel.css"

const Carousel = ({
	initialIndex = 0,
	transitionDuration = 400,
	autoplay = true,
	autoplayInterval = 3000,
	infiniteLoop = true,
	onPageChange = () =>console.log("no function"),
	children}) => {

	const [current, setCurrent] = useState(initialIndex);
	
	const childNodes = children ? children.length ? children : [children] : 0;
	
	const PlayPrevious = () => {
		if (infiniteLoop){
			if(current === 0)  return setCurrent(childNodes.length - 1)
			else  setCurrent(current - 1)
		} 
		else {
			if(current === 0)  return setCurrent(0) 
			else  setCurrent(current -1)
		}
	}

	const PlayNext = () => {
		if (infiniteLoop){
			if(current === childNodes.length - 1)  return setCurrent(0) 
			else  setCurrent(current + 1)
		} 
		else {
			if(current === childNodes.length - 1)  return setCurrent(childNodes.length - 1) 
			else  setCurrent(current + 1)
		}
	}

useEffect(()=> {
		if (childNodes.length > 1 && autoplay) {

			if (childNodes[current].type !== "video"){
				const mediaChangeInterval = setInterval(() => {PlayNext()}, autoplayInterval);
				return ()=> clearInterval(mediaChangeInterval)
			}
		}
	})

	useEffect(()=> {
		onPageChange()
	// eslint-disable-next-line react-hooks/exhaustive-deps
	},[current])

  return (
    childNodes.length > 0 && (
      <div className="carousel-wrapper">

				<CarouselButton onClick={() => PlayPrevious()}>Prev</CarouselButton>

				<CarouselItem 
					key={current}
					type={childNodes[current].type} 
					src={
						childNodes[current].type === "video"
						? childNodes[current].props.children[0].props.src
						: childNodes[current].props.src
					}
					alt={childNodes[current].props.alt}
					transitionTime={transitionDuration}
				/>

				<CarouselButton onClick={() => PlayNext()}>Next</CarouselButton>

      </div>	
    )
  );
};

export default Carousel;