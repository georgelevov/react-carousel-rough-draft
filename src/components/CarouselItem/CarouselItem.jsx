// Disabled since code is only for demo purposes
/* eslint-disable react/prop-types */
import "./CarouselItem.css"

const CarouselItem = ({type, src, alt, transitionTime }) => {

  if (type==="img") {
		return (
			<img 
				className="carousel-item scale-in-center"
				src={src}
				alt={alt}
				style={{animation:`scale-in-center ${transitionTime}ms cubic-bezier(0.250, 0.460, 0.450, 0.940) both`}}/>
		)
	}
	if (type==="video") {
		return (
			<video
				className="carousel-item scale-in-center"
				controls autoPlay loop muted
				style={{animation:`scale-in-center ${transitionTime}ms cubic-bezier(0.250, 0.460, 0.450, 0.940) both`}}>
				<source src={src} type="video/mp4"/>
			</video>
		)
	}

}

export default CarouselItem;