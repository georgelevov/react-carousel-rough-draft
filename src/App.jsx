// Homework - Carousel
// The assignment purpose is to implement a simple Carousel in React with following features:

// Support Autoplay
// Infinite Loop of Items
// Support Image and video as Item

// The component should expose the following props as configuration options:
// initialIndex?: number, defaults to 0
// transitionDuration?: number, defaults to 400ms
// autoplay?: boolean, defaults to true
// autoplayInterval?: number, defaults to 3000ms
// infiniteLoop?: boolean, defaults to true\
// onPageChange?(index: number): void;


import './App.css'
import Carousel from './components/Carousel/Carousel';


import image1 from "../src/assets/image_1_sml.jpg"
import image2 from "../src/assets/image_2_sml.jpg"
import image3 from "../src/assets/image_3_sml.jpg"
import image4 from "../src/assets/image_4_sml.jpg"
import image5 from "../src/assets/image_5_sml.jpg"
import video1 from "../src/assets/video_1.mp4"

function App() {

	const pageChangedMessage = ()=> console.log("Page Changed")

  return (
    <>
		{/* Accepts IMG and VIDEO tags as Children */}
		<Carousel
			// initialIndex={}
			// transitionDuration={}
			// autoplay={true}
			// autoplayInterval={}
			// infiniteLoop={true}
			onPageChange={pageChangedMessage}
		>
			<img src={image1} alt=""/>
			<img src={image2} alt=""/>
			<video controls autoPlay muted>
				<source src={video1} type="video/mp4"/>
				This browser does not display the video tag.
			</video>
			<img src={image3} alt=""/>
			<img src={image4} alt=""/>
			<img src={image5} alt=""/>	
		</Carousel>
    </>
  )
}

export default App
