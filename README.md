# How to test the Project

> Assuming you have *git bash* or any other console with **NPM** installed

1. In console navigate to main project dir
2. Type 'npm install', *please wait until project is done installing dependencies*
3. Type 'npm run dev'
4. Project will launch, please use **Local:** link to open in browser

## How to use the component

1. The compontent name is **Carousel**, you can find it inside (*main dir*).src/components/Carousel/Carousel.jsx 
2. You can include any image HTML tag or video HTML tag as a child to the Carousel component
6. The Carousel will display the content in the order it has been added, (descending order).